# Hangman

# Is about guessing a random word with a limited number of guesses.
# Consider the following requirements with your game:
# Max number of guesses per user
# Add a timer restriction of 30 seconds per user to guess a word
# Player should be able to input guesses

from random import choice
from time import sleep, monotonic
from msvcrt import kbhit, getwche

# ------------- Improvise input with timer of the Game ----------------

class TimeoutExpired(Exception):
    pass

def input_with_timeout(prompt, timeout, timer=monotonic):
    print(prompt, end = "")                 # Print prompt
    endtime = timer() + timeout             # Set timer
    result = []
    while timer() < endtime:                # Happens till endtime
        if kbhit():                         # Keyboard Hit
            result.append(getwche())        # Add Unicode into List
            if result[-1] == '\r':          # True when 'carriage return'
                return ''.join(result[:-1]) # Return joined list
        sleep(0.04)                         # Just to yield to other processes/threads
    raise TimeoutExpired                    # Triggers when while conditions satisfied

# ---------------------------------------------------------------------

# Hangman Game Initialization
words = ["eggyolk","eggnog","eggshell"]     # words were hardcoded. Can be dynamic
guess = ""
guessed_letters = ""
already_guessed = []
length = 0

lives = 5                                   # User have only 5 lives
failed_attempts = 0
time_limit = 30                             # Timer set to 30 sec

# Intro to game
print(f'''Welcome to Hangman Game!
Game Mechanics:
* {lives} lives before getting hangged
* {time_limit} seconds per user to guess a word
Let's Go? (y if YES, n if NO)''')

while True:
    game = input()
    if game.strip() == "n":
        continue
    elif game.strip() == "y":
        print("\nThe game is about to start!\n")
        break
    else:
        continue

# Select a word to guess
word = choice(words)
hidden_word = word
length = len(word)
guessed_letters = display = '_'*len(word)

# Extra delay before the game
sleep(2)

# Start of the game
while True:
    # Check if word is correct already
    if hidden_word == guessed_letters:
        print(f'''
        You are correct, the word was {hidden_word}.
        You are now safe!
        ''')
        break
    else:
        try:
            print(f"Missing word : {guessed_letters}")

            # Improvised Buffer
            guess = input_with_timeout(f"Enter your guess w/ letters : ",
                time_limit)
            print()

        # Execute when the time runs-out
        except TimeoutExpired:
            
            print('''
            
                        Sorry, times up!

                             _____
                            |     |   
                            |     |
                            |     |
                            |     O      
                            |    /|\\
                            |    / \\
                          __|__
            
                You are hanged!!! Game Over!

            ''')
            break
        else:
            # Validator
            guess = guess.strip()
            if len(guess.strip()) == 0 or len(guess.strip()) >= 2 or guess <= "9":
                print("\nInvalid input, try a letter.\n")
            
            elif guess in word:
                already_guessed.extend([guess])
                index = word.find(guess)
                word = word[:index] + "_" + word[index + 1:]
                guessed_letters = guessed_letters[:index] + guess + guessed_letters[index + 1:]
            
            elif guess in already_guessed:
                print("\n\nLetter/s guessed. Try another letter.\n")

            else:
                failed_attempts += 1

                if failed_attempts == 1:
                    # time.sleep(1)
                    print('''
                             _____
                            |     
                            |      
                            |      
                            |      
                            |      
                            |      
                          __|__
                    ''')
                    print("Wrong guess. " + str(lives - failed_attempts) + " guesses remaining\n")

                elif failed_attempts == 2:
                    # time.sleep(1)
                    print('''
                             _____
                            |     |   
                            |     |
                            |      
                            |      
                            |      
                            |      
                          __|__
                    ''')
                    print("Wrong guess. " + str(lives - failed_attempts) + " guesses remaining\n")

                elif failed_attempts == 3:
                    # time.sleep(1)
                    print('''
                             _____
                            |     |   
                            |     |
                            |     |
                            |      
                            |      
                            |      
                          __|__
                    ''')
                    print("Wrong guess. " + str(lives - failed_attempts) + " guesses remaining\n")

                elif failed_attempts == 4:
                    # time.sleep(1)
                    print('''
                             _____
                            |     |   
                            |     |
                            |     |
                            |     O      
                            |      
                            |      
                          __|__
                    ''')
                    print("Wrong guess. " + str(lives - failed_attempts) + " last guess remaining\n")

                elif failed_attempts == 5:
                    # time.sleep(1)
                    print('''
                             _____
                            |     |   
                            |     |
                            |     |
                            |     O      
                            |    /|\\
                            |    / \\
                          __|__
                    ''')
                    print("Wrong guess. You are hanged!!!\n")
                    print(f"The word {guessed_letters} was : {hidden_word}\n")
                    break
        
# End of the game
